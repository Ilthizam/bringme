import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Modal,
  ActivityIndicator,
} from "react-native";
import Colors from "./Colors";
// import Fire from "./Fire";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import MyDrawer from "./components/MyDrawer";
import LoginScreen from "./screens/LoginScreen";
import firebase from "firebase";

import { firebaseConfig } from "./config";
firebase.initializeApp(firebaseConfig);

// const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default class App extends React.Component {
  state = {
    isLoggedIn: false,
  };

  checkIsLoggedIn = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ isLoggedIn: true });
      }
    });
  };
  logOut = () => {
    console.log("LOgout presses");
    this.setState({ isLoggedIn: false });

    firebase.auth().signOut();
  };

  componentDidMount() {
    this.checkIsLoggedIn();
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          {this.state.isLoggedIn ? (
            <>
              <Stack.Screen
                name="Home"
                component={MyDrawer}
                initialParams={{ itemId: this.logOut }}
              />
            </>
          ) : (
            <Stack.Screen name="SignIn" component={LoginScreen} />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    );

    // return (
    //   <NavigationContainer>
    //     <MyDrawer />
    //   </NavigationContainer>
    // );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    borderWidth: 2,
    borderColor: Colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: Colors.blue,
    fontWeight: "600",
    fontSize: 14,
    marginTop: 8,
  },
});

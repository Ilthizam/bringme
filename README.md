# BringMe Expo App

Keep track your shopping list.



## Run the application 

The minimum supported OS versions is Android 5+.

1. [Download](https://drive.google.com/file/d/1xp9HIzid_Q74Qsx6qQ-tNhTd44zhx6ua/view?usp=sharing)  the application APK and install it. No any special permission required. 
2. Install application APK on the android mobile phone





## **Demo video**

Demo video is available [from here ](https://drive.google.com/file/d/1USY6SbpXqgoHDUCBCcuv-RdE6kaa13Kj/view?usp=sharing)

---

# Setting up the project and excute 

1. Clone the repository
2. Install dependencies 
```
    npm install
```

---
title: Set up the expo project and execute. 
---

There are two tools that you need to develop apps with Expo: a local development tool and a mobile client to open your app.

## 1. Local development tool: Expo CLI

Expo CLI is a tool for developing apps with Expo. In addition to the command-line interface (CLI) it also has a web-based graphical user interface (GUI) that pops up in your web browser when you start your project &mdash; you can use this if you're not yet comfortable with the using terminal or just prefer GUIs, both have similar capabilities.

### Pre-requisities

- **Node.js**: In order to install Expo CLI you will need to have Node.js (**we recommend the latest stable version**- but the maintenance and active LTS releases will also work) installed on your computer. [Download the recommended version of Node.js](https://nodejs.org/en/).
- **Git**: Additionally, you'll need Git to create new projects. [You can download Git from here](https://git-scm.com).

### Installing Expo CLI

We recommend installing Expo CLI globally, you can do this by running the following command:

**(Expo version 3.21.5 for unbreakable changes)**
```
npm install -g expo-cli
```


## 2. Mobile app: Expo client for Android

Expo client is the tool you will use to run your projects while you're developing them. When you serve your project with Expo CLI, it generates a development URL that you can open in Expo client to preview your app.

- 🤖 [Download Expo client for Android from the Play Store](https://play.google.com/store/apps/details?id=host.exp.exponent)


> ⚠️ **Required operating system versions:** The minimum Android version is Lollipop (5) and the minimum iOS version is iOS 10.0.




---
title: Android Studio Emulator
---

If you don't have an Android device available to test with, we recommend using the default emulator that comes with Android Studio. If you run into any problems setting it up, follow the steps in this guide.

## Step 1: Set up Android Studio's tools

- [Download](https://developer.android.com/studio) and install Android Studio 3.0+.

- Go to Preferences -> Appearance & Behavior -> System Settings -> Android SDK. Click on the "SDK Tools" tab and make sure you have at least one version of the "Android SDK Build-Tools" installed.

![Android SDK location](https://docs.expo.io/static/images/android-studio-build-tools.png)

- Copy or remember the path listed in the box that says "Android SDK Location."

![Android SDK location](https://docs.expo.io/static/images/android-studio-sdk-location.png)

- If you are on macOS or Linux, add the Android SDK location to your PATH using `~/.bash_profile` or `~/.bashrc`. You can do this by adding a line like `export ANDROID_SDK=/Users/myuser/Library/Android/sdk`.

- On macOS, you will also need to add `platform-tools` to your `~/.bash_profile` or `~/.bashrc.`, by adding a line like `export PATH=/Users/myuser/Library/Android/sdk/platform-tools:$PATH`

> Note that later versions of macOS, such as Catalina, use `zsh` instead of `bash`, so you will update `.zprofile` or `.zshrc` instead.

- Make sure that you can run `adb` from your terminal.

## Step 2: Set up a virtual device

- From the Android Studio main screen, go to `Tools -> AVD Manager`.

- Press the "+ Create Virtual Device" button.

![Android SDK location](https://docs.expo.io/static/images/android-studio-avd-manager.png)

- Choose the type of hardware you'd like to emulate. We recommend testing against a variety of devices, but if you're unsure where to start, the newest device in the Pixel line could be a good choice.

- Select an OS version to load on the emulator (probably one of the system images in the "Recommended" tab), and download the image.

- Change any other settings you'd like, and press "Finish" to create the virtual device. You can now run this device anytime by pressing the Play button in the AVD Manager window.

#### Multiple `adb` versions

Having multiple `adb` versions on your system can result in the error `adb server version (xx) doesn't match this client (xx); killing...`

This is because the adb version on your system is different from the adb version on the android sdk platform-tools.

- Open the terminal and check the `adb` version on the system:

`$adb version`

- And from the Android SDK platform-tool directory:

`$cd ~/Android/sdk/platform-tools`

`$./adb version`

- Copy `adb` from Android SDK directory to `usr/bin` directory:

`$sudo cp ~/Android/sdk/platform-tools/adb /usr/bin`


---
 **title : Running on a android device**
---



npm start --localhost --android



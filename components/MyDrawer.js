import React from "react";
import { View, ImageBackground, Text, StyleSheet } from "react-native";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";
import HomeScreen from "../screens/HomeScreen";

import Colors from "../Colors";
import MyGroupScreen from "../screens/MyGroupScreen";
import InpirationScreen from "../screens/InspirationScreen";
import SharedShoppingListScreen from "../screens/SharedShoppingListScreen";

const Drawer = createDrawerNavigator();

const image = {
  uri:
    "https://firebasestorage.googleapis.com/v0/b/bringme-c690c.appspot.com/o/209929ec4cb31c68eac8e9f05c23a7c2.png?alt=media&token=104f4cc4-f046-46bd-82a0-5efa0c290c83",
};
const home = {
  uri:
    "https://firebasestorage.googleapis.com/v0/b/bringme-c690c.appspot.com/o/faceless-man-cartoon-chef-character-uniform-holds-huge-recipe-empty-list-dreaming-pizza-fast-food-snack-mockup-176418650.jpg?alt=media&token=d56dbfbe-f2c6-4a1b-87f2-84d4fe9aa060",
};
const work = {
  uri:
    "https://png.pngtree.com/thumb_back/fw800/back_our/20190621/ourmid/pngtree-taobao-tmall-home-decoration-festival-banner-template-image_190319.jpg",
};

export default class MyDrawer extends React.Component {
  state = {
    title: this.props,
  };

  logOut = () => {
    const { itemId } = this.props.route.params;
    return itemId;
  };

  render() {
    return (
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={(props) => (
          <CustomDrawerContent {...props} logOut={this.logOut()} />
        )}
      >
        <Drawer.Screen name="Home" component={HomeScreen} />
        {/* <Drawer.Screen
          name="Shared List"
          component={SharedShoppingListScreen}
        /> */}
        <Drawer.Screen name="My Groups" component={MyGroupScreen} />
        <Drawer.Screen name="Inspiration" component={InpirationScreen} />
      </Drawer.Navigator>
    );
  }
}

function CustomDrawerContent(props) {
  const { logOut } = props;

  return (
    <DrawerContentScrollView {...props}>
      <View style={{ flex: 1 }}>
        <ImageBackground source={image} style={styles.image}>
          <Text style={styles.title}>
            Bring
            <Text
              style={{
                fontWeight: "300",
                color: Colors.blue,
              }}
            >
              Me
            </Text>
          </Text>
        </ImageBackground>
      </View>
      <DrawerItemList {...props} />
      <ImageBackground source={home} style={styles.image2}>
        {/* <Text style={styles.text}>Home</Text> */}
      </ImageBackground>
      {/* <ImageBackground source={work} style={styles.image}>
        <Text style={styles.text}>Workplace</Text>
      </ImageBackground> */}
      <DrawerItem
        label="Log out"
        onPress={() => {
          logOut();
        }}
      />
    </DrawerContentScrollView>
  );
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    height: 200,
    alignItems: "center",
  },
  image2: {
    height: 280,
  },
  text: {
    // color: "white",
    fontSize: 30,
    fontWeight: "bold",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.white,
    paddingHorizontal: 64,
  },
});

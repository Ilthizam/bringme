import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Image,
  ScrollView,
  Modal,
} from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";
import { SearchBar } from "react-native-elements";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
} from "native-base";
import firebase from "firebase";
import "@firebase/firestore";
import _ from "lodash";

export default class RecipeModal extends React.Component {
  state = {
    data: this.props.data,
  };

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          justifyContent: "flex-start",
          alignItems: "center",
        }}
      >
        <ScrollView>
          <View>
            <Image
              style={styles.tinyLogo}
              source={{
                uri: this.state.data.image,
              }}
            />
            <Text
              style={{
                color: Colors.black,
                fontSize: 30,
                marginHorizontal: 20,
              }}
            >
              {this.state.data.name}
            </Text>
            <Text
              style={{
                color: Colors.black,
                fontSize: 15,
                marginHorizontal: 20,
              }}
            >
              {this.state.data.shortDes}
            </Text>
            <Text
              style={{
                color: Colors.black,
                fontSize: 15,
                marginHorizontal: 15,
                marginTop: 30,
                textAlign: "auto",
                width: 390,
              }}
            >
              {this.state.data.instructions}
            </Text>
          </View>
        </ScrollView>
        {/* <View style={[styles.section, { flex: 3 }]}>
            <Image
              style={styles.tinyLogo}
              source={{
                uri: this.state.data.image,
              }}
            />
            <Text>{this.state.data.name}</Text>
            <Text>{this.state.data.shortDes}</Text>
          </View> */}

        {/* <View style={[styles.footer, { margin: 16 }]}></View> */}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "red",
  },
  tinyLogo: {
    width: 450,
    height: 350,
  },
  input: {
    // flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addUser: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  members: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
    paddingLeft: 20,
  },
});

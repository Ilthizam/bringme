import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Modal } from "react-native";
import Colors from "../Colors";
import EditShoppingListItemModal from "./EditShoppingListItemModal";

export default class ShoppingList extends React.Component {
  state = {
    showListVisible: false,
  };

  toggleListModal() {
    this.setState({ showListVisible: !this.state.showListVisible });
  }

  render() {
    const list = this.props.list;
    const completedCount = list.todos.filter((todo) => todo.completed).length;
    const remainingCount = list.todos.length - completedCount;
    // const completedCount = 10;
    // const remainingCount = 20;
    // console.log(list)

    return (
      <View>
        <Modal
          animationType="slide"
          visible={this.state.showListVisible}
          onRequestClose={() => this.toggleListModal()}
        >
          <EditShoppingListItemModal
            list={list}
            closeModal={() => this.toggleListModal()}
            updateList={this.props.updateList}
            deleteList={this.props.deleteList}
            shareAccess={this.props.shareAccess}
          />
        </Modal>
        <TouchableOpacity
          onLongPress={() => alert("Are you sure you want to delete?")}
          onPress={() => this.toggleListModal()}
          style={[styles.listContainer, { backgroundColor: list.color }]}
        >
          <Text style={styles.listTitle} numberOfLines={1}>
            {list.name}
          </Text>

          <View>
            <View style={{ alignItems: "center" }}>
              <Text style={styles.count}>{remainingCount}</Text>
              <Text style={styles.subTitle}>Remaining</Text>
            </View>
            <View style={{ alignItems: "center" }}>
              <Text style={styles.count}>{completedCount}</Text>
              <Text style={styles.subTitle}>Completed</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  listContainer: {
    paddingVertical: 32,
    paddingHorizontal: 16,
    borderRadius: 6,
    marginHorizontal: 12,
    alignItems: "center",
    width: 200,
  },
  listTitle: {
    fontSize: 24,
    fontWeight: "700",
    color: Colors.white,
    marginBottom: 18,
  },
  completed: {},
  count: {
    fontSize: 48,
    fontWeight: "200",
    color: Colors.white,
  },
  subTitle: {
    fontSize: 12,
    fontWeight: "700",
    color: Colors.white,
  },
});

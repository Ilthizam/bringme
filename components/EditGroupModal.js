import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Image,
  Modal,
} from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";
import _ from "lodash";
import AddMemberModal from "./AddMemberModal";
import SharedShoppingList from "./SharedShoppingList";

import SharedShoppingListScreen from "../screens/SharedShoppingListScreen";
import GroupListModal from "./GroupListModal";

export default class EditGroupModal extends React.Component {
  state = {
    name: "",
    email: "",
    validated: false,
    users: [],
    toggleVisible: false,
    toggleListVisible: false,
    list: this.props.list,
  };

  toggleToBuyCompleted = (index) => {
    let list = this.props.list;
    list.members[index].completed = !list.members[index].completed;

    this.props.updateGroup(list);
  };

  toggleListVisibleModal() {
    this.setState({
      toggleListVisible: !this.state.toggleListVisible,
    });
  }
  toggleVisibleModal() {
    this.setState({ toggleVisible: !this.state.toggleVisible });
  }
  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // alert("Email is Not Correct");
      this.setState({ email: text });
      return false;
    } else {
      this.setState({ email: text });
      this.setState({ validated: true });

      // alert("Email is Correct");
    }
  };

  addUser = () => {
    let list = this.props.list;
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (
      this.state.name != "" &&
      this.state.email != "" &&
      this.state.validated
    ) {
      list.members.push({
        name: this.state.name,
        email: this.state.email,
      });

      this.props.updateGroup(list);
      this.setState({ name: "", email: "" });
      Keyboard.dismiss();
    } else {
      alert("Check you inputs");
    }
  };
  removeMember = (member) => {
    // firebase.removeProple(member, this.props.list);
    // console.log(_.filter(data, { state: "New York" }));
    // console.log(
    //   _.filter(data, (e) => {
    //     return e.state !== "New York";
    //   })
    // );

    const list = this.state.list;
    const updateMembers = _.filter(this.state.list.members, (e) => {
      return e.id !== member.id;
    });
    list["members"] = updateMembers;

    // console.log(list);
    this.setState({ list: list });
    this.props.removeMember(list, member);
  };

  renderMembers = (members, index) => {
    // console.log(members)
    return (
      <View style={styles.toBuyContainer}>
        <TouchableOpacity
          // onLongPress={() => alert("Are you sure you want to delete?")}
          onPress={() => this.toggleToBuyCompleted(index)}
        >
          <Image
            style={styles.tinyLogo}
            source={{
              uri: members.picUrl,
            }}
          />
        </TouchableOpacity>
        <Text style={[styles.members]}>
          {members.name + "\n" + members.email}
        </Text>
        <TouchableOpacity
          style={{ flex: 1, flexDirection: "row-reverse" }}
          onPress={() => this.removeMember(members)}
        >
          <Text style={{ color: Colors.red, fontSize: 12, fontWeight: "bold" }}>
            Remove
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const list = this.state.list;

    const members = list.members.length;
    // const completedCount = list.members.filter((todo) => todo.completed).length;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <Modal
          onRequestClose={() => this.toggleVisibleModal()}
          animationType="slide"
          visible={this.state.toggleVisible}
        >
          <AddMemberModal
            list={list}
            closeModal={() => this.toggleVisibleModal()}
          />
        </Modal>
        <Modal
          onRequestClose={() => this.toggleListVisibleModal()}
          animationType="slide"
          visible={this.state.toggleListVisible}
        >
          <GroupListModal
            list={list}
            closeModal={() => this.toggleListVisibleModal()}
            updateList={this.updateList}
            deleteList={this.deleteList}
          />
        </Modal>
        <SafeAreaView
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              position: "absolute",
              top: 64,
              right: 32,
              zIndex: 10,
            }}
          >
            <AntDesign
              name="close"
              size={24}
              color={Colors.black}
              onPress={this.props.closeModal}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.section,
              styles.header,
              { borderBottomColor: list.color },
            ]}
          >
            <View>
              <Text style={styles.title}>{list.name}</Text>
              <Text style={styles.taskCount}>{members} members</Text>
            </View>
          </View>
          <View style={[styles.section, { flex: 3 }]}>
            <FlatList
              data={list.members}
              keyExtractor={(_, index) => index.toString()}
              renderItem={({ item, index }) => this.renderMembers(item, index)}
              contentContainerStyle={{
                paddingHorizontal: 32,
                paddingVertical: 64,
              }}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
            />
          </View>

          <View style={[styles.footer, { margin: 16 }]}>
            {/* <TextInput
              style={[styles.input, { borderColor: list.color }]}
              onChangeText={(text) => this.setState({ name: text })}
              value={this.state.name}
              placeholder="Name"
            /> */}
          </View>
          <View style={[styles.footer, { margin: 16, marginLeft: 200 }]}>
            {/* <TextInput
              style={[styles.input, { borderColor: list.color }]}
              onChangeText={(text) => this.validate(text)}
              placeholder="Email ID"
              value={this.state.email}
            /> */}
            <TouchableOpacity
              onPress={() => this.toggleVisibleModal()}
              style={[styles.addUser, { backgroundColor: list.color }]}
            >
              <Text style={{ color: "white" }}>Add people</Text>
              {/* <AntDesign name="plus" size={16} color={Colors.white} /> */}
            </TouchableOpacity>
            <View
              style={{
                padding: 16,
                marginHorizontal: 0,
              }}
            ></View>
            <TouchableOpacity
              onPress={() => this.toggleListVisibleModal()}
              style={[
                styles.addUser,
                { backgroundColor: list.color, marginLeft: -30 },
              ]}
            >
              <Text style={{ color: "white" }}>Shared lists</Text>
              {/* <AntDesign name="minus" size={16} color={Colors.white} /> */}
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "red",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  input: {
    flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addUser: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  members: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
    paddingLeft: 20,
  },
});

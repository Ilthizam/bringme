import React from "react";

import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  FlatList,
  View,
} from "react-native";
import {
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
} from "native-base";

import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";
import Fire from "../Fire";

export default class SelectGroupModal extends React.Component {
  state = {
    toggleVisible: false,
    groups: [],

    loading: true,
  };

  componentDidMount() {
    firebase = new Fire((error, user) => {
      if (error) {
        return alert("Uh no, something went wrong");
      }
      firebase.getGroups((groups) => {
        // this.setState({ groups, user }, () => {
        //   // this.setState({ loading: false });
        // });
        this.setState({ groups });
        // console.log(groups);
      });

      // this.setState({ user });
    });
  }

  componentWillUnmount() {
    firebase.detach();
  }
  rendeItem = (item, index) => {
    return (
      <List>
        <ListItem thumbnail>
          <Left>
            <Thumbnail
              square
              source={{
                uri: "https://picsum.photos/200/300",
              }}
            />
          </Left>
          <Body>
            <Text>
              Group Name : <Text style={{fontSize:18}} >{item.name}</Text>
            </Text>
            <Text note numberOfLines={1}>
              {item.members.length} members
            </Text>
          </Body>
          <Right>
            <Button transparent onPress={() => this.shareList(item)}>
              <Text>Share</Text>
            </Button>
          </Right>
        </ListItem>
      </List>
    );
  };

  shareList = (item) => {
    // console.log(item);
    // console.log(this.props.lists);
    firebase.shareGroups(item.id, this.props.lists);
  };

  render() {
    const groups = this.state.groups;

    return (
      <SafeAreaView
        style={{
          flex: 1,
          //   justifyContent: "center",
          //   alignItems: "center",
        }}
      >
        <TouchableOpacity
          style={{
            position: "absolute",
            top: 64,
            right: 32,
            zIndex: 10,
          }}
        >
          <AntDesign
            name="close"
            size={24}
            color={Colors.black}
            onPress={this.props.closeModal}
          />
        </TouchableOpacity>
        <View style={{ marginTop: 100 }}>
          {groups.length == 0 ? (
            <Text style={{alignSelf:'center', fontSize:16}} >You don't have any group. Add a group first</Text>
          ) : (
            <FlatList
              data={groups}
              renderItem={({ item, index }) => this.rendeItem(item, index)}
              keyExtractor={(_, index) => index.toString()}
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addToBuy: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  toBuy: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
  },
});

import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Button,
  Modal,
} from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";
import Constants from "expo-constants";
import SelectGroupModal from "./SelectGroupModal";
import _ from "lodash";

export default class EditShoppingListItemModal extends React.Component {
  state = {
    newToDo: "",
    toggleVisible: false,
  };

  toggleToBuyCompleted = (index) => {
    let list = this.props.list;
    list.todos[index].completed = !list.todos[index].completed;

    this.props.updateList(list);
  };

  deleteItem = (item) => {
    let list = this.props.list;

    const todos = _.filter(list.todos, (e) => {
      return e.title !== item.title;
    });

    list.todos = todos;
    // console.log(list);

    this.props.updateList(list);
  };

  addToBuy = () => {
    let list = this.props.list;

    if (this.state.newToDo != "") {
      list.todos.push({
        title: this.state.newToDo,
        completed: false,
      });

      this.props.updateList(list);
      this.setState({ newToDo: "" });
      Keyboard.dismiss();
    }
  };

  deleteList = (list) => {
    this.props.deleteList(list.id);
  };

  toggleVisibleModal() {
    this.setState({ toggleVisible: !this.state.toggleVisible });
  }
  renderToBuy = (toBuy, index) => {
    return (
      <View style={styles.toBuyContainer}>
        <TouchableOpacity onPress={() => this.toggleToBuyCompleted(index)}>
          <Ionicons
            name={toBuy.completed ? "ios-square" : "ios-square-outline"}
            size={24}
            color={Colors.gray}
            style={{ width: 32 }}
          />
        </TouchableOpacity>
        <Text
          style={[
            styles.toBuy,
            {
              textDecorationLine: toBuy.completed ? "line-through" : "none",
            },
            {
              color: toBuy.completed ? Colors.gray : Colors.black,
            },
          ]}
        >
          {toBuy.title}
        </Text>
        <TouchableOpacity
          style={{ flex: 1, flexDirection: "row-reverse" }}
          onPress={() => this.deleteItem(toBuy)}
        >
          <Text
            style={{
              color: Colors.red,
              fontSize: 12,
              fontWeight: "bold",
            }}
          >
            Remove
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const list = this.props.list;
    const taskCount = list.todos.length;
    const completedCount = list.todos.filter((todo) => todo.completed).length;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <Modal
          onRequestClose={() => this.toggleVisibleModal()}
          animationType="slide"
          visible={this.state.toggleVisible}
        >
          <SelectGroupModal
            closeModal={() => this.toggleVisibleModal()}
            lists={list}
            // addList={this.addList}
            // closeParentModal={() => this.props.toggleListModal}
          />
        </Modal>
        <SafeAreaView
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              position: "absolute",
              top: 64,
              right: 32,
              zIndex: 10,
            }}
          >
            <AntDesign
              name="close"
              size={24}
              color={Colors.black}
              onPress={this.props.closeModal}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.section,
              styles.header,
              { borderBottomColor: list.color },
            ]}
          >
            <View>
              <Text style={styles.title}>{list.name}</Text>
              <Text style={styles.taskCount}>
                {completedCount} of {taskCount} items
              </Text>
            </View>
          </View>
          <View style={[styles.section, { flex: 3 }]}>
            <FlatList
              data={list.todos}
              keyExtractor={(_, index) => index.toString()}
              renderItem={({ item, index }) => this.renderToBuy(item, index)}
              contentContainerStyle={{
                paddingHorizontal: 32,
                paddingVertical: 64,
              }}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
            />
          </View>

          <View style={[styles.section, styles.footer]}>
            <TextInput
              style={[styles.input, { borderColor: list.color }]}
              onChangeText={(text) => this.setState({ newToDo: text })}
              value={this.state.newToDo}
            />
            <TouchableOpacity
              onPress={() => this.addToBuy()}
              style={[styles.addToBuy, { backgroundColor: list.color }]}
            >
              <AntDesign name="plus" size={16} color={Colors.white} />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        <View
          style={{
            flexDirection: "row",
            alignSelf: "center",
          }}
        >
          <View
            style={{
              padding: 2,
              flex: 1,
              marginHorizontal: 0,
            }}
          >
            <Button
              onPress={() => this.deleteList(list)}
              title="Delete List"
              color={Colors.red}
              accessibilityLabel="Learn more about this purple button"
            />
          </View>
          {this.props.shareAccess === true ? (
            <View
              style={{
                padding: 2,
                flex: 1,
                marginHorizontal: 0,
              }}
            >
              <Button
                title="Share with Groups"
                color={Colors.lightBlue}
                accessibilityLabel="Learn more about this purple button"
                onPress={() => this.toggleVisibleModal()}
              />
            </View>
          ) : null}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addToBuy: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  toBuy: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
  },
});

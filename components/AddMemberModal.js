import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Image,
  Modal,
} from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";
import { SearchBar } from "react-native-elements";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Body,
  Right,
  Button,
} from "native-base";
import firebase from "firebase";
import "@firebase/firestore";
import _ from "lodash";

export default class AddMemberModal extends React.Component {
  state = {
    search: "",
    data: [],
    fullData: [],
    query: "",
    list: this.props.list,
    members: this.props.list.members,
  };

  const;

  componentDidMount() {
    this.getUsers();
  }
  getUsers = () => {
    let ref = firebase.firestore().collection("users");
    const currentUser = firebase.auth().currentUser;
    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];
      snapshot.forEach((doc) => {
        if (doc.id != currentUser.uid) {
          lists.push({ id: doc.id, ...doc.data() }); 
        }
      });
      this.setState({ data: lists, fullData: lists });
    });
  };
  updateSearch = (text) => {
    const formattedQuerry = text.toLowerCase();
    const data = _.filter(this.state.fullData, (user) => {
      if (user.gmail.includes(formattedQuerry)) {
        return true;
      }
      return false;
    });
    this.setState({ data, query: text, search: text });
  };
 
  inviteUser = (user) => {
    const temp = this.state.members;
    const currentUser = firebase.auth().currentUser;
    const tempIds = [];
    temp.forEach((element) => {
      tempIds.push(element.id);
    });
    if (!tempIds.includes(user.id)) {
      temp.push({
        id: user.id,
        email: user.gmail,
        name: user.name,
        picUrl: user.picUrl,
      });

      this.setState({ members: temp });

      temp.forEach((element) => {
        //update other user
        firebase
          .firestore()
          .collection("users")
          .doc(element.id)
          .collection("groups")
          .doc(this.state.list.id)
          .set({
            members: temp,
            accepted: false,
            color: this.props.list.color, 
            name: this.props.list.name,
            id: this.state.list.id,
          });
      });

      // //update current user
      firebase
        .firestore()
        .collection("users")
        .doc(currentUser.uid)
        .collection("groups")
        .doc(this.state.list.id)
        .update({
          members: temp,
          accepted: true,
        });
    }

    this.props.closeModal();
  };

  _renderItem = ({ item, index }) => {
    return (
      <ListItem thumbnail>
        <Left>
          <Thumbnail
            // square
            source={{
              uri: item.picUrl,
            }}
          />
        </Left>
        <Body>
          <Text>{item.name}</Text>
          <Text note numberOfLines={1}>
            {item.gmail}
          </Text>
        </Body>
        <Right>
          <Button onPress={() => this.inviteUser(item)} transparent>
            <Text>Add user</Text>
          </Button>
        </Right>
      </ListItem>
    );
  };

  render() {
    const { search } = this.state;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <SafeAreaView
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {/* <TouchableOpacity
            style={{
              position: "absolute",
              top: 64,
              right: 32,
              zIndex: 10,
            }}
          >
            <AntDesign
              name="close"
              size={24}
              color={Colors.black}
              onPress={this.props.closeModal}
            />
          </TouchableOpacity>

          <View style={[styles.section, styles.header]}>
            <View>
              <Text>dsad</Text> 
            </View>
          </View> */}
          <View style={[styles.section, { flex: 3 }]}>
            <SearchBar
              placeholder="Type Here..."
              onChangeText={this.updateSearch}
              value={search}
            />

            <List>
              <FlatList
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
              />
            </List>
          </View>

          <View style={[styles.footer, { margin: 16 }]}></View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "red",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  input: {
    // flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addUser: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  members: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
    paddingLeft: 20,
  },
});

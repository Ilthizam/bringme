import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Button,
} from "react-native";
import Colors from "../Colors";
import EditGroupModal from "./EditGroupModal";

export default class GroupList extends React.Component {
  state = {
    showListVisible: false,
  };

  toggleListModal() {
    const list = this.props.list;
    if (list.accepted) {
      this.setState({
        showListVisible: !this.state.showListVisible,
      });
    }
  }

  acceptInvitation = (list) => {
    firebase.acceptInvitation(list);
  };
  rejectInvitation = (list) => {
    firebase.rejectInvitation(list);
    // console.log(_.filter(data, { state: "New York" }));
    // console.log(
    //   _.filter(data, (e) => {
    //     return e.state !== "New York";
    //   })
    // );

    // console.log(list);
  };

  render() {
    const list = this.props.list;

    const count = list.members.length;
    // const remainingCount = list.todos.length - completedCount;

    return (
      <View>
        <Modal
          animationType="slide"
          visible={this.state.showListVisible}
          onRequestClose={() => this.toggleListModal()}
        >
          <EditGroupModal
            list={list}
            closeModal={() => this.toggleListModal()}
            updateGroup={this.props.updateGroup}
            removeMember={this.props.removeMember}
          />
        </Modal>
        <TouchableOpacity
          onPress={() => this.toggleListModal()}
          style={[styles.listContainer, { backgroundColor: list.color }]}
        >
          <Text style={styles.listTitle} numberOfLines={1}>
            {list.name}
          </Text>

          <View>
            <View style={{ alignItems: "center" }}>
              <Text style={styles.count}>{count}</Text>
              <Text style={styles.subTitle}>People</Text>
              {!list.accepted ? (
                <View style={{ margin: 10, padding: 10 }}>
                  <Button
                    onPress={() => this.acceptInvitation(list)}
                    title="Accpet"
                    color={Colors.black}
                    accessibilityLabel="Learn more about this purple button"
                  />
                  <View style={{ margin: 3 }} />
                  <Button
                    onPress={() => this.rejectInvitation(list)}
                    title="Reject"
                    color={Colors.red}
                    accessibilityLabel="Learn more about this purple button"
                  />
                </View>
              ) : null}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  listContainer: {
    paddingVertical: 32,
    paddingHorizontal: 16,
    borderRadius: 6,
    marginHorizontal: 12,
    alignItems: "center",
    width: 200,
  },
  listTitle: {
    fontSize: 24,
    fontWeight: "700",
    color: Colors.white,
    marginBottom: 18,
  },
  completed: {},
  count: {
    fontSize: 48,
    fontWeight: "200",
    color: Colors.white,
  },
  subTitle: {
    fontSize: 12,
    fontWeight: "700",
    color: Colors.white,
  },
});

import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  Image,
  Modal,
} from "react-native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import Colors from "../Colors";

import AddMemberModal from "./AddMemberModal";
import SharedShoppingList from "./SharedShoppingList";
import firebase from "firebase";
import "@firebase/firestore";
import SharedShoppingListScreen from "../screens/SharedShoppingListScreen";
import ShoppingList from "./ShoppingList";
import { set } from "react-native-reanimated";

export default class GroupListModal extends React.Component {
  state = {
    datas: [],
  };

  componentDidMount() {
    firebase
      .firestore()
      .collection("groups")
      .doc(this.props.list.id)
      .collection("lists")
      .onSnapshot((snapshot) => {
        lists = [];
        snapshot.forEach((doc) => {
          // console.log(doc.data());
          lists.push({ id: doc.id, ...doc.data() });
        });
        this.setState({ datas: lists });
        // console.log(this.state.lists);
      });
  }

  updateList = (list) => {
    firebase
      .firestore()
      .collection("groups")
      .doc(this.props.list.id)
      .collection("lists")
      .doc(list.id)
      .update(list);
  };
  deleteList = (listId) => {
    firebase
      .firestore()
      .collection("groups")
      .doc(this.props.list.id)
      .collection("lists")
      .doc(listId)
      .delete();
  };

  renderList = (list) => {
    // console.log(list);
    return (
      <ShoppingList
        list={list}
        updateList={this.updateList}
        deleteList={this.deleteList}
        shareAccess={false}
      />
    );
  };

  render() {
    const data = this.state.datas;
    // console.log(this.state.lists);
    return (
      <SafeAreaView
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          style={{
            position: "absolute",
            top: 64,
            right: 32,
            zIndex: 10,
          }}
        >
          <AntDesign
            name="close"
            size={24}
            color={Colors.black}
            onPress={this.props.closeModal}
          />
        </TouchableOpacity>

        <View
          style={[
            styles.section,
            styles.header,
            { borderBottomColor: data.color },
          ]}
        >
          <View>
            <Text style={styles.title}>Shared Lists</Text>
            <Text style={styles.taskCount}> members</Text>
          </View>
        </View>
        <View style={[styles.section, { flex: 3 }]}>
          <View style={{ height: 275, paddingLeft: 32 }}>
            {data.length == 0 ? (
              <Image
                style={styles.tinyLogo}
                source={{
                  uri:
                    "https://firebasestorage.googleapis.com/v0/b/bringme-c690c.appspot.com/o/ae8ac2fa217d23aadcc913989fcc34a2.png?alt=media&token=898007ba-3a60-4adc-810b-d9f5c2640c22",
                }}
              />
            ) : (
              <FlatList
                data={data}
                keyExtractor={(item) => item.id.toString()}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => this.renderList(item)}
              />
            )}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  section: {
    flex: 1,
    alignSelf: "stretch",
  },
  header: {
    justifyContent: "flex-end",
    marginLeft: 64,
    borderBottomWidth: 3,
    marginBottom: 20,
  },
  title: {
    fontSize: 30,
    fontWeight: "800",
    color: Colors.black,
  },
  taskCount: {
    marginTop: 4,
    marginBottom: 16,
    color: Colors.gray,
    fontWeight: "600",
  },
  footer: {
    paddingHorizontal: 32,
    flexDirection: "row",
    alignItems: "center",
    // backgroundColor: "red",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  input: {
    flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8,
  },
  addUser: {
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  toBuyContainer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignItems: "center",
  },
  members: {
    color: Colors.black,
    fontWeight: "700",
    fontSize: 16,
    paddingLeft: 20,
  },
  tinyLogo: {
    flex:1,
    alignSelf:'center',
    width: 450,
    height: 200,
    
  },
});

import firebase from "firebase";
import "@firebase/firestore";
import _ from "lodash";

const firebaseConfig = {
  apiKey: "AIzaSyCrJCBZ3I7hLBnSODUwk20TtEOCzFiwkWQ",
  authDomain: "bringme-c690c.firebaseapp.com",
  databaseURL: "https://bringme-c690c.firebaseio.com",
  projectId: "bringme-c690c",
  storageBucket: "bringme-c690c.appspot.com",
  messagingSenderId: "335887134223",
  appId: "1:335887134223:web:b634c0e82ba14c1b063d7a",
  measurementId: "G-HH8ZPBS7XJ",
};

class Fire {
  constructor(callback) {
    this.init(callback);
  }

  init(callback) {
    // if (!firebase.apps.length) {
    //   firebase.initializeApp(firebaseConfig);
    // }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        callback(null, user);
      }
      //  else {

      //   firebase
      //     .auth()
      //     .signInAnonymously()
      //     .catch((error) => {
      //       callback(error);
      //     });
      // }
    });
  }

  get userId() {
    return firebase.auth().currentUser.uid;
  }

  get ref() {
    return firebase.firestore().collection("users").doc(this.userId);
  }

  // isLoggedIn(){

  // }

  //Shopping list

  addList(list) {
    let ref = this.ref.collection("lists");
    ref.add(list);
  }
  getLists(callback) {
    let ref = this.ref.collection("lists").orderBy("name");

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];
      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });
      callback(lists);
    });
  }
  updateList(list) {
    let ref = this.ref.collection("lists");
    ref.doc(list.id).update(list);
  }
  deleteList(id) {
    let ref = this.ref.collection("lists");
    ref.doc(id).delete();
  }

  //group list

  addGroup(list) {
    const currentUser = firebase.auth().currentUser;
    const uuid = uuidv4();

    let ref = this.ref.collection("groups");
    list.members.push({
      email: currentUser.email,
      name: currentUser.displayName,
      picUrl: currentUser.photoURL,
      id: currentUser.uid,
    });
    list["id"] = uuid;

    // ref.add(list);

    ref.doc(uuid).set(list);

    firebase.firestore().collection("groups").doc(uuid).set({ id: uuid });
  }

  acceptInvitation(list) {
    let ref = this.ref.collection("groups");
    ref.doc(list.id).update({
      accepted: true,
    });
  }

  rejectInvitation(list) {
    const currentUser = firebase.auth().currentUser;

    const members = _.filter(list.members, (e) => {
      return e.id !== currentUser.uid;
    });

    list["members"] = members;
    list["accepted"] = true;

    let ref = this.ref.collection("groups");

    // list["id"] = uuid;

    ref
      .doc(list.id)
      .delete()
      .then(
        list.members.forEach((element) => {
          firebase
            .firestore()
            .collection("users")
            .doc(element.id)
            .collection("groups")
            .doc(list.id)
            .update(list);
        })
      );
    console.log(list);
  }
  removeMember(list, member) {
    firebase
      .firestore()
      .collection("users")
      .doc(member.id)
      .collection("groups")
      .doc(list.id)
      .delete()
      .then(
        list.members.forEach((element) => {
          firebase
            .firestore()
            .collection("users")
            .doc(element.id)
            .collection("groups")
            .doc(list.id)
            .update({
              color: list.color,
              id: list.id,
              members: list.members,
              name: list.name,
            });
        })
      );
  }

  shareGroups(groupID, list) {
    // console.log(groupID)
    // console.log(list)
    let ref = this.ref.collection("lists");

    firebase
      .firestore()
      .collection("groups")
      .doc(groupID)
      .collection("lists")
      .doc(list.id)
      .set(list)
      .then(ref.doc(list.id).delete())
      .then(alert("Successfully shared"));
  }

  getUsrs(callback) {
    let ref = firebast.firestore().collection("users");

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];
      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });
      callback(lists);
    });
  }

  getGroups(callback) {
    let ref = this.ref.collection("groups").orderBy("name");

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];
      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });
      callback(lists);
    });
  }

  updateGroup(list) {
    let ref = this.ref.collection("groups");
    ref.doc(list.id).update(list);
  }

  getRecipes(callback) {
    let ref = firebase.firestore().collection("recipes");

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      lists = [];

      snapshot.forEach((doc) => {
        lists.push({ id: doc.id, ...doc.data() });
      });
      callback(lists);
    });
  }

  detach() {
    this.unsubscribe();
  }
}
function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export default Fire;

export default tempData = [
  {
    id: 1,
    name: "one",
    color: "#24A6D9",
    todos: [
      {
        title: "Book a flight",
        completed: false,
      },
      {
        title: "Two",
        completed: true,
      },
      {
        title: "Threee",
        completed: true,
      },
      {
        title: "Four",
        completed: false,
      },
    ],
  },
  {
    id: 2,
    name: "two",
    color: "#8022D9",
    todos: [
      {
        title: "Book a flight",
        completed: false,
      },
      {
        title: "Two",
        completed: false,
      },
      {
        title: "Threee",
        completed: true,
      },
      {
        title: "Four",
        completed: false,
      },
    ],
  },

  {
    id: 3,
    name: "three",
    color: "#595BD9",
    todos: [
      {
        title: "Book a flight",
        completed: false,
      },
      {
        title: "Two",
        completed: true,
      },
      {
        title: "Threee",
        completed: true,
      },
      {
        title: "Four",
        completed: false,
      },
    ],
  },
];

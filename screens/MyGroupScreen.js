import React from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text,
  FlatList,
  Modal,
  Image,
  ActivityIndicator,
} from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import Colors from "../Colors";
import { AntDesign } from "@expo/vector-icons";
import GroupList from "../components/GroupList";

import Fire from "../Fire";
import AddGroupModal from "../components/AddGroupModal";

export default class MyGroupScreen extends React.Component {
  state = {
    toggleVisible: false,
    lists: [],
    user: {},
    loading: true,
  };
  componentDidMount() {
    firebase = new Fire((error, user) => {
      if (error) {
        return alert("Uh no, something went wrong");
      }
      firebase.getGroups((lists) => {
        this.setState({ lists, user }, () => {
          this.setState({ loading: false });
        });
      });

      this.setState({ user });
    });
  }

  componentWillUnmount() {
    firebase.detach();
  }
  toggleVisibleModal() {
    this.setState({ toggleVisible: !this.state.toggleVisible });
  }
  renderList = (list) => {
    return (
      <GroupList
        list={list}
        updateGroup={this.updateGroup}
        removeMember={this.removeMember}
      />
    );
  };

  addGroup = (list) => {
    firebase.addGroup({
      name: list.name,
      color: list.color,
      members: [],
      accepted: true,
    });
  };

  updateGroup = (list) => {
    firebase.updateGroup(list);
  };
  removeMember = (list, member) => {
    firebase.removeMember(list, member);
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={[
            styles.container,
            { alignItems: "center", justifyContent: "center" },
          ]}
        >
          <ActivityIndicator size="large" color={Colors.blue} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Modal
          onRequestClose={() => this.toggleVisibleModal()}
          animationType="slide"
          visible={this.state.toggleVisible}
        >
          <AddGroupModal
            closeModal={() => this.toggleVisibleModal()}
            addGroup={this.addGroup}
          />
        </Modal>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{ alignItems: "flex-end", margin: 16 }}
            onPress={() => this.props.navigation.toggleDrawer()}
          >
            <FontAwesome5
              name="bars"
              size={24}
              color="#161924"
              style={{ margin: 15 }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={styles.divider} />
              <Text style={styles.title}>
                Bring
                <Text
                  style={{
                    fontWeight: "300",
                    color: Colors.blue,
                  }}
                >
                  Me
                </Text>
                <Text style={{ fontSize: 10 }}>Groups</Text>
              </Text>
              <View style={styles.divider} />
            </View>
            <View style={{ marginVertical: 48 }}>
              <TouchableOpacity
                style={styles.addGroup}
                onPress={() => this.toggleVisibleModal()}
              >
                <AntDesign
                  name="plus"
                  size={16}
                  color={Colors.backgroundColor}
                />
              </TouchableOpacity>
              <Text style={styles.add}>Add Group</Text>
            </View>
            <View style={{ height: 275, paddingLeft: 32 }}>
              {this.state.lists.length == 0 ? (
                <Image
                  style={styles.tinyLogo}
                  source={{
                    uri:
                      "https://firebasestorage.googleapis.com/v0/b/bringme-c690c.appspot.com/o/ae8ac2fa217d23aadcc913989fcc34a2.png?alt=media&token=898007ba-3a60-4adc-810b-d9f5c2640c22",
                  }}
                />
              ) : (
                <FlatList
                  data={this.state.lists}
                  keyExtractor={(item) => item.id.toString()}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({ item }) => this.renderList(item)}
                />
              )}
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: "#161924",
    fontSize: 20,
    fontWeight: "500",
  },
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  // container: {
  //   flex: 1,
  //   backgroundColor: "#fff",
  // alignItems: "center",
  // justifyContent: "center",
  // },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  addGroup: {
    borderWidth: 2,
    borderColor: Colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: Colors.blue,
    fontWeight: "600",
    fontSize: 14,
    marginTop: 8,
  },
  tinyLogo: {
    opacity: 1,
    width: 450,
    height: 200,
    margin: 50,
  },
});

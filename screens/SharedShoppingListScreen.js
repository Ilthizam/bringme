import React from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import Colors from "../Colors";
import ShoppingList from "../components/ShoppingList";
import tempData from "../tempData";
import firebase from "firebase";
import "@firebase/firestore";
import SharedShoppingList from "../components/SharedShoppingList";
export default class SharedShoppingListScreen extends React.Component {
  state = {
    lists: [],
    loading: true,
  };

  componentDidMount() {
    this.getSharedGroups();
  }
  componentWillUnmount() {
    this.unsubscribe();
  }

  getSharedGroups = () => {
    let currentUser = firebase.auth().currentUser;
    this.unsubscribe = firebase
      .firestore()
      .collection("users")
      .doc(currentUser.uid)
      .collection("groups")
      .onSnapshot((snapshot) => {
        idArray = [];
        snapshot.forEach((doc) => {
          idArray.push({ id: doc.id, ...doc.data() });
        });
        this.getSharedLists(idArray);
      });
  };

  getSharedLists = (groupIdList) => {
    lists = [];

    groupIdList.forEach((element) => {
      // console.log(element['id']);

      firebase
        .firestore()
        .collection("groups")
        .doc(element["id"])
        .onSnapshot((snapshot) => {
          // snapshot.forEach((doc) => {

          if (snapshot.data()) lists.push(snapshot.data());
          // console.log(lists);
          this.setState({ lists: lists });
          this.setState({ loading: false });

          // });
        });
    });
    // console.log(lists);
  };

  renderList = (list) => {
    return (
      <SharedShoppingList
        list={list}
        updateList={this.updateList}
        deleteList={this.deleteList}
      />
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={[
            styles.container,
            { alignItems: "center", justifyContent: "center" },
          ]}
        >
          <ActivityIndicator size="large" color={Colors.blue} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{ alignItems: "flex-end", margin: 16 }}
            onPress={() => this.props.navigation.toggleDrawer()}
          >
            <FontAwesome5
              name="bars"
              size={24}
              color="#161924"
              style={{ margin: 15 }}
            />
          </TouchableOpacity>

          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={styles.divider} />
              <Text style={styles.title}>
                Bring
                <Text
                  style={{
                    fontWeight: "300",
                    color: Colors.blue,
                  }}
                >
                  Me
                </Text>
              </Text>
              <View style={styles.divider} />
            </View>
            <View style={{ marginVertical: 48 }}>
              {/* <TouchableOpacity
                style={styles.addList}
                onPress={() => this.toggleVisibleModal()}
              >
                <AntDesign
                  name="plus"
                  size={16}
                  color={Colors.backgroundColor}
                />
              </TouchableOpacity>
              <Text style={styles.add}>Add List</Text> */}
            </View>
            <View style={{ height: 275, paddingLeft: 32 }}>
              <FlatList
                data={this.state.lists}
                keyExtractor={(item) => item.id.toString()}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item }) => this.renderList(item)}
              />
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
  },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    borderWidth: 2,
    borderColor: Colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: Colors.blue,
    fontWeight: "600",
    fontSize: 14,
    marginTop: 8,
  },
});

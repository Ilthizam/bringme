import React from "react";

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Modal,
  SafeAreaView,
  ActivityIndicator,
  Button,
  Image,
} from "react-native";
import Colors from "../Colors";
import { AntDesign } from "@expo/vector-icons";

import { FontAwesome5 } from "@expo/vector-icons";
import firebase from "firebase";
import "@firebase/firestore";
// import * as Google from "expo-google-app-auth";
import * as Google from "expo-google-app-auth";
import * as AppAuth from "expo-app-auth"; // you will use this in your logInAsync method

export default class LoginScreen extends React.Component {
  isUserEqual = (googleUser, firebaseUser) => {
    if (firebaseUser) {
      var providerData = firebaseUser.providerData;
      for (var i = 0; i < providerData.length; i++) {
        if (
          providerData[i].providerId ===
            firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
          providerData[i].uid === googleUser.getBasicProfile().getId()
        ) {
          // We don't need to reauth the Firebase connection.
          return true;
        }
      }
    }
    return false;
  };
  onSignIn = (googleUser) => {
    console.log("Google Auth Response", googleUser);
    // We need to register an Observer on Firebase Auth to make sure auth is initialized.
    var unsubscribe = firebase.auth().onAuthStateChanged(
      function (firebaseUser) {
        unsubscribe();
        // Check if we are already signed-in Firebase with the correct user.
        if (!this.isUserEqual(googleUser, firebaseUser)) {
          // Build Firebase credential with the Google ID token.
          var credential = firebase.auth.GoogleAuthProvider.credential(
            googleUser.idToken,
            googleUser.accessToken
          );
          // Sign in with credential from the Google user.
          firebase
            .auth()
            .signInWithCredential(credential)
            .then(function (result) {
              console.log("user signed in");
              if (result.additionalUserInfo.isNewUser) {
                firebase
                  .firestore()
                  .collection("users")
                  .doc(result.user.uid)
                  .set({
                    gmail: result.user.email,
                    picUrl: result.user.photoURL,
                    name: result.user.displayName,
                    createdAt: Date.now(),
                  });
              } else {
                firebase
                  .firestore()
                  .collection("users")
                  .doc(result.user.uid)
                  .update({
                    lastLoggedIn: Date.now(),
                  });
              }
            })
            .catch(function (error) {
              // Handle Errors here.
              var errorCode = error.code;
              var errorMessage = error.message;
              // The email of the user's account used.
              var email = error.email;
              // The firebase.auth.AuthCredential type that was used.
              var credential = error.credential;
              // ...
            });
        } else {
          console.log("User already signed-in Firebase.");
        }
      }.bind(this)
    );
  };

  signInWithGoogleAsync = async () => {
    try {
      const result = await Google.logInAsync({
        // behavior: "web",
        androidClientId:
          "335887134223-539nddbf4ic85rnl94ccfn8u30d34974.apps.googleusercontent.com",
        androidStandaloneAppClientId:
          "335887134223-pfsmfqard5n564a53vsqr7uedk6lhcer.apps.googleusercontent.com",
        // iosClientId: YOUR_CLIENT_ID_HERE,
        scopes: ["profile", "email"],
        // redirectUrl: `${AppAuth.OAuthRedirect}:/oauth2redirect/google`, // this is the LINE
      });

      if (result.type === "success") {
        this.onSignIn(result);
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={styles.divider} />
              <Text style={styles.title}>
                Bring
                <Text
                  style={{
                    fontWeight: "300",
                    color: Colors.blue,
                  }}
                >
                  Me
                </Text>
              </Text>
              <View style={styles.divider} />
            </View>
            <Text style={{fontSize:15}} >Ilthizam Imtiyas - 17000599</Text>
            <View style={{ marginVertical: 48, marginBottom: -100 }}>
              <Button
                rounded
                title="Login with Google"
                onPress={() => this.signInWithGoogleAsync()}
              />
            </View>
          </View>
          <Image
            style={styles.tinyLogo}
            source={{
              uri:
                "https://static.vecteezy.com/system/resources/previews/000/180/956/non_2x/supermarket-vector.jpg",
            }}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
  },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    borderWidth: 2,
    borderColor: Colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: Colors.blue,
    fontWeight: "600",
    fontSize: 14,
    marginTop: 8,
  },
  tinyLogo: {
    width: 450,
    height: 400,
  },
});

import React from "react";
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  ImageBackground,
  Modal,
  ScrollView,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FontAwesome5 } from "@expo/vector-icons";
import Colors from "../Colors";
import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
  CardButton,
  CardImage,
} from "react-native-cards";
import Fire from "../Fire";
import RecipeModal from "../components/RecipeModal";

// const image = {
//   uri:
//     "https://previews.123rf.com/images/topform8/topform81610/topform8161000011/66668657-cooking-food-set-icons-in-sketch-style.jpg",
// };

export default class InpirationScreen extends React.Component {
  state = {
    toggleVisible: false,
    lists: [],
    loading: true,
    selectedItem: {},
  };

  componentDidMount() {
    firebase = new Fire((error, user) => {
      if (error) {
        return alert("Uh no, something went wrong");
      }
      firebase.getRecipes((lists) => {
        this.setState({ lists }, () => {
          this.setState({ loading: false });
        });
      });
    });
  }

  renderRecipe = (item) => {
    return (
      <ScrollView>
        <Card style={{ borderRadius: 40, elevation: 20, margin: 25 }}>
          <CardImage
            style={{ borderRadius: 40, margin: 20 }}
            source={{ uri: item.image }}
            title={item.name}
          />
          {/* <CardTitle subtitle="Number 6" /> */}
          <CardContent text={item.shortDes} />
          <CardAction separator={true} inColumn={false}>
            <CardButton
              onPress={() => {
                this.addList(item);
                this.props.navigation.navigate("Home");
              }}
              title="Add to shopping List"
              color="#FEB557"
            />
            <CardButton
              onPress={() => {
                this.setState({ selectedItem: item });
                this.toggleVisibleModal();
              }}
              title="Explore"
              color="#FEB557"
            />
          </CardAction>
        </Card>
      </ScrollView>
    );
  };
  componentWillUnmount() {
    firebase.detach();
  }

  toggleVisibleModal() {
    this.setState({ toggleVisible: !this.state.toggleVisible });
  }

  addList = (list) => {
    firebase.addList({
      name: list.name,
      color: Colors.blue,
      todos: list.ingredients,
    });
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={[
            styles.container,
            { alignItems: "center", justifyContent: "center" },
          ]}
        >
          <ActivityIndicator size="large" color={Colors.blue} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <ImageBackground source={image} style={styles.image}> */}
        <Modal
          onRequestClose={() => this.toggleVisibleModal()}
          animationType="slide"
          visible={this.state.toggleVisible}
        >
          <RecipeModal
            data={this.state.selectedItem}
            closeModal={() => this.toggleVisibleModal()}
          />
        </Modal>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{ alignItems: "flex-end", margin: 16 }}
            onPress={() => this.props.navigation.toggleDrawer()}
          >
            <FontAwesome5
              name="bars"
              size={24}
              color="#161924"
              style={{ margin: 15 }}
            />
          </TouchableOpacity>
          <View style={{ alignSelf: "center" }}>
            <View style={styles.divider} />

            <Text style={styles.title}>
              Bring
              <Text
                style={{
                  fontWeight: "300",
                  color: Colors.blue,
                }}
              >
                Me
              </Text>
              <Text style={{ fontSize: 10 }}>Inspiration</Text>
            </Text>
            <View style={styles.divider} />
          </View>
          <FlatList
            style={{ marginTop: 50 }}
            data={this.state.lists}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => this.renderRecipe(item)}
          />
        </SafeAreaView>
        {/* </ImageBackground> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    opacity: 0.2,
    justifyContent: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    // flex: 1,
    alignSelf: "center",
  },
});

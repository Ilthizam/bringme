import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Modal,
  Image,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";
import Colors from "../Colors";
import { AntDesign } from "@expo/vector-icons";
import ShoppingList from "../components/ShoppingList";
import AddShoppingListModal from "../components/AddShoppingListModal";
import Fire from "../Fire";
import { FontAwesome5 } from "@expo/vector-icons";

export default class HomeScreen extends React.Component {
  state = {
    toggleVisible: false,
    lists: [],
    user: {},
    loading: true,
  };

  componentDidMount() {
    
    firebase = new Fire((error, user) => {
      if (error) {
        return alert("Uh no, something went wrong");
      }
      firebase.getLists((lists) => {
        this.setState({ lists, user }, () => {
          this.setState({ loading: false });
        });
      });
      this.setState({ user });
    });
  }

  componentWillUnmount() {
    firebase.detach();
  }
  toggleVisibleModal() {
    this.setState({ toggleVisible: !this.state.toggleVisible });
  }
  renderList = (list) => {
    return (
      <ShoppingList
        list={list}
        updateList={this.updateList}
        deleteList={this.deleteList}
        shareAccess={true}
      />
    );
  };

  addList = (list) => {
    firebase.addList({
      name: list.name,
      color: list.color,
      todos: [],
    });
  };

  updateList = (list) => {
    firebase.updateList(list);
  };
  deleteList = (id) => {
    firebase.deleteList(id);
  };

  render() {
    if (this.state.loading) {
      return (
        <View
          style={[
            styles.container,
            { alignItems: "center", justifyContent: "center" },
          ]}
        >
          <ActivityIndicator size="large" color={Colors.blue} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Modal
          onRequestClose={() => this.toggleVisibleModal()}
          animationType="slide"
          visible={this.state.toggleVisible}
        >
          <AddShoppingListModal
            closeModal={() => this.toggleVisibleModal()}
            addList={this.addList}
          />
        </Modal>
        <SafeAreaView style={{ flex: 1 }}>
          <TouchableOpacity
            style={{ alignItems: "flex-end", margin: 16 }}
            onPress={() => this.props.navigation.toggleDrawer()}
          >
            <FontAwesome5
              name="bars"
              size={24}
              color="#161924"
              style={{ margin: 15 }}
            />
          </TouchableOpacity>
          <View style={{ alignSelf: "center" }}>
            <Text style={{ fontSize: 20, fontWeight: "500" }}>
              Hello ! {this.state.user.displayName}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={styles.divider} />
              <Text style={styles.title}>
                Bring
                <Text
                  style={{
                    fontWeight: "300",
                    color: Colors.blue,
                  }}
                >
                  Me
                </Text>
                <Text style={{ fontSize: 10 }}>Lists</Text>
              </Text>
              <View style={styles.divider} />
            </View>
            <Text style={{ fontSize: 15 }}>Ilthizam Imtiyas - 17000599</Text>
            <View style={{ marginVertical: 48 }}>
              <TouchableOpacity
                style={styles.addList}
                onPress={() => this.toggleVisibleModal()}
              >
                <AntDesign
                  name="plus"
                  size={16}
                  color={Colors.backgroundColor}
                />
              </TouchableOpacity>
              <Text style={styles.add}>Add List</Text>
            </View>
            <View style={{ height: 275, paddingLeft: 32 }}>
              {this.state.lists.length == 0 ? (
                <Image
                  style={styles.tinyLogo}
                  source={{
                    uri:
                      "https://firebasestorage.googleapis.com/v0/b/bringme-c690c.appspot.com/o/ae8ac2fa217d23aadcc913989fcc34a2.png?alt=media&token=898007ba-3a60-4adc-810b-d9f5c2640c22",
                  }}
                />
              ) : (
                // <Text>fsdfsdf</Text>
                <FlatList
                  data={this.state.lists}
                  keyExtractor={(item) => item.id.toString()}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({ item }) => this.renderList(item)}
                />
              )}
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
  },
  divider: {
    backgroundColor: Colors.lightBlue,
    height: 1,
    flex: 1,
    alignSelf: "center",
  },
  title: {
    fontSize: 38,
    fontWeight: "800",
    color: Colors.black,
    paddingHorizontal: 64,
  },
  addList: {
    borderWidth: 2,
    borderColor: Colors.lightBlue,
    borderRadius: 4,
    padding: 16,
    alignItems: "center",
    justifyContent: "center",
  },
  add: {
    color: Colors.blue,
    fontWeight: "600",
    fontSize: 14,
    marginTop: 8,
  },
  tinyLogo: {
    opacity: 1,
    width: 450,
    height: 200,
    flex: 1,
    alignSelf: "center",
    // margin:50
  },
});
